<?php

namespace Tests\Feature\Vendor\Nsru\Health;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ConfigurationTest extends TestCase
{
    public function test_ตรวจสอบ_timezone_เป็น_asia_bangkok()
    {
        $this->assertContains('Asia/Bangkok', [ \config('app.timezone') ], 'Timezone ปัจจุบันคือ '.\config('app.timezone'). ' ต้องกำหนดเป็น Asia/Bangkok');
    }

    public function test_ตั้งค่า_debug_เป็น_false()
    {
        $this->assertFalse(\config('app.debug'), 'การตั้งค่า debug ควรเป็น false');
    }

    public function test_ตั้งค่า_env_เป็น_production()
    {
        $this->assertFalse(\config('app.env'), 'การตั้งค่า env ควรเป็น production');
    }

    public function test_ตรวจสอบโฟลเดอร์_storage_ให้สามารถเขียนและลบไฟล์ได้()
    {
        $this->assertDirectoryIsWritable(\storage_path(''));
        $this->assertDirectoryIsWritable(\storage_path('app'));
        $this->assertDirectoryIsWritable(\storage_path('app/public'));
        $this->assertDirectoryIsWritable(\storage_path('logs'));
    }

    public function test_ตรวจสอบการทำ_storage_link()
    {
        $this->assertDirectoryIsWritable(\public_path('storage'));
    }

}
