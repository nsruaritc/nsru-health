<?php

namespace Nsru\Health\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class VendorNsruHealthCmdRecord extends Model
{
    use HasFactory;

    public $casts = [
        'process_name'      => 'string',
        'begin_datetime'    => 'datetime',
        'end_datetime'      => 'datetime',
        'is_success'        => 'boolean',
        'late_minutes'      => 'integer'
    ];

    public function getIsLateAttribute() {
        /** @var \DateInterval */
        $diff = $this->end_datetime->diff(Carbon::now());
        $seconds = ( $diff->h * 3600 ) + ( $diff->i * 60 ) + $diff->s;
        return ( $seconds >= $this->late_minutes );
    }

    public static function begin(string $name, int $lateMinutes = 90) {
        $record = new VendorNsruHealthCmdRecord();
        $record->process_name   = $name;
        $record->begin_datetime = Carbon::now();
        $record->late_minutes   = $lateMinutes;
        return $record;
    }

    public function end(bool $isSuccess) {
        $this->is_success   = $isSuccess;
        $this->end_datetime = Carbon::now();
        $this->save();
    }
}
