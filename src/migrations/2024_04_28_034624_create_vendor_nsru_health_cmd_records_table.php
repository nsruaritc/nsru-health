<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorNsruHealthCmdRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_nsru_health_cmd_records', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('process_name')->comment('ชื่อการประมวลผล');
            $table->datetime('begin_datetime')->comment('วันเวลาที่เริ่มการประมวลผล');
            $table->dateTime('end_datetime')->comment('วันเวลาที่การประมวลผลเสร็จสิ้น');
            $table->boolean('is_success')->comment('สถานะการประมวลผลสำเร็จ');
            $table->integer('late_minutes')->comment('จำนวนนาทีที่ถือว่ามีปัญหา หลังจากการประมวลผลครั้งสุดท้าย');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_nsru_health_cmd_records');
    }
}
