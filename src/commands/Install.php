<?php

namespace Nsru\Health\Commands;

use App\Models\User;
use DateTime;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nsru-health:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Napadon Admin in project.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('================================================');
        $this->info('WELCOME TO NSRU HEALTH INSTALL COMMAND');
        $this->info('================================================');

        \sleep(1);

        // if($this->confirm("Publishing Assets ?", false)) {
        //     $this->isPublishingAssets = true;
        // }

        // if($this->confirm("NPM Process ?", false)) {
        //     $this->isNpmProcess = true;
        // }


        /* =====================================================
         * การดำเนินการที่เกี่ยวข้องกับการแก้ไขไฟล์
         * ===================================================== */

        // $this->isDelayForWritingToDisk = false;

        /*
         * แนะนำให้เอาอันนี้ขึ้นก่อนเพราะมีผลต่อคำสั่งชุดหลังๆ
         */
        // $this->updateFileContent(
        //     base_path('app/Models/User.php'),
        //     "use HasApiTokens, HasFactory, Notifiable;",
        //     "use HasApiTokens, HasFactory, Notifiable;\n\tuse \Spatie\Permission\Traits\HasRoles;",
        //     "use \Spatie\Permission\Traits\HasRoles;"
        // );

        // $this->updateFileContent(
        //     base_path('routes/api.php'),
        //     null,
        //     '\Napadon\Admin\NapadonAdmin::apiRoutes();'
        // );

        // $this->updateFileContent(
        //     base_path('routes/web.php'),
        //     null,
        //     '\Napadon\Admin\NapadonAdmin::webRoutes();'
        // );

        // เว้นเวลาไว้ให้ระบบเขียนไฟล์ให้ครบถ้วน
        // if($this->isDelayForWritingToDisk) {
        //     $this->line('Wait 5 seconds for writing change');
        //     $bar = $this->output->createProgressBar(5);
        //     $bar->start();
        //     for($i = 1; $i <= 5; $i++) {
        //         \sleep(1);
        //         $bar->advance();
        //     }
        //     $bar->finish();
        //     $this->line('');
        // }

        // $this->line('Publishing nsru/health files');
        // $this->callSilent('vendor:publish', [ '--tag' => 'nsru-health', '--force' => true  ]);

        // Migrate database.
        // $this->line('Migrating the database tables into your application');
        // $this->callSilent('migrate');

        // Seed
        // $this->callSilent('db:seed', [ '--class' => '\Napadon\Admin\Database\Seeders\ProvinceSeeder' ]); // ตัวอย่างการเรียก Seed แบบที่เป็น Seed ของ Laravel
        // $this->line('Seeding all reference data');
        // Seed แบบที่เขียนเป็นฟังก์ชันเอง
        // $this->syncGeographyDataFromOnlineDatasource($this);
        // $this->syncProvinceDataFromOnlineDatasource($this);
        // $this->syncDistrictDataFromOnlineDatasource($this);
        // $this->syncSubDistrictDataFromOnlineDatasource($this);

        // Create default user.
        // $this->createUser('system@local',     'System');
        // $this->createUser('developer@local',  'Developer',        'developer');
        // $this->createUser('admin@local',      'Administrator',    'admin');

        // Publishing spatie/permission
        // $this->line('Publishing spatie/permission assets, database, and config files');
        // $this->callSilent('vendor:publish', ['--provider' => 'Spatie\Permission\PermissionServiceProvider', '--force' => true]);
        // $this->callSilent('config:clear');
        // $this->callSilent('migrate');

        // Add role
        // $this->line('Add role');
        // $this->callSilent( 'permission:create-role', [ 'name' => 'Developer'     ] );
        // $this->callSilent( 'permission:create-role', [ 'name' => 'Administrator' ] );
        // $this->callSilent( 'permission:create-role', [ 'name' => 'User'          ] );

        // Add role to Developer User
        // $this->line('Add role to Developer User');
        // $developer = User::where('email', 'developer@local')->first();
        // $developer->assignRole('User');
        // $developer->assignRole('Administrator');
        // $developer->assignRole('Developer');

        // Add role to Admin User
        // $this->line('Add role to Administrator User');
        // $admin = User::where('email', 'admin@local')->first();
        // $admin->assignRole('User');
        // $admin->assignRole('Administrator');


        // Set Timezone
        // $this->updateFileContent(
        //     \config_path('app.php'),
        //     "'timezone' => 'UTC'",
        //     "'timezone' => 'Asia/Bangkok'"
        // );

        // Set Locale
        // $this->updateFileContent(
        //     \config_path('app.php'),
        //     "'locale' => 'en'",
        //     "'locale' => 'th'"
        // );

        // Vite
        // $this->updateFileContent(
        //     base_path('vite.config.js'),
        //     "'resources/js/app.js'",
        //     "'resources/js/app.js', 'vendor/napadon/admin/src/resources/css/app.sass', 'vendor/napadon/admin/src/resources/js/app.js'",
        //     'vendor/napadon/admin/src/resources/css/app.sass'
        // );

        // NPM
        // if($this->isNpmProcess) {
        //     \exec('npm install');
        //     \exec('npm install sass');
        //     \exec('npm run build');
        // }

        $this->line('Thank you. Good bye. See you again Tomorrow.');
        $this->line('----------');

        return Command::SUCCESS;
    }

}
