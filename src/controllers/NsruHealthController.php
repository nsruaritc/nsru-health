<?php

namespace Nsru\Health\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Nsru\Health\Models\VendorNsruHealthCmdRecord;

class NsruHealthController extends Controller
{
    public function __invoke()
    {
        $up             = true;
        $cmds           = [];
        foreach(VendorNsruHealthCmdRecord::distinct('process_name')->pluck('process_name') as $processName) {
            $cmdRecord = VendorNsruHealthCmdRecord::where('process_name', $processName)->orderBy('end_datetime', 'desc')->first();
            $cmds[] = [
                'process_name'          => $processName,
                'last_end_datetime'     => $cmdRecord->end_datetime,
                'last_result'           => $cmdRecord->is_success,
                'is_late'               => $cmdRecord->is_late,
                'has_problem'           => ( ! $cmdRecord->is_success ) || $cmdRecord->is_late
            ];
        }

        $processTime    = (float)number_format(microtime(true) - LARAVEL_START, 3);
        return \response()->json([
            'up'            => $up,
            'process_time'  => $processTime,
            'cmd'           => $cmds
        ]);
    }
}
