<?php

use Illuminate\Support\Facades\Route;
use Nsru\Health\Controllers\NsruHealthController;

Route::get('/vendor/nsru/health', NsruHealthController::class);
