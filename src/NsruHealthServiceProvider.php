<?php
namespace Nsru\Health;

use Illuminate\Support\ServiceProvider;
use Nsru\Health\Commands\Install as InstallCommand;

class NsruHealthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [

    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $router = $this->app['router'];

        // โหลดไฟล์ Migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        // โหลดไฟล์ภาษา
        // $this->loadTranslationsFrom(__DIR__.'/lang', 'napadon-admin');

        // ลงทะเบียน Middleware
        // $router->aliasMiddleware('auth.napadon-admin', \Napadon\Admin\App\Http\Middleware\Authenticate::class);

        $this->loadRoutesFrom(__DIR__.'/routes/web.php');

        // ลงทะเบียน Component
        // Blade::componentNamespace('Napadon\Admin\App\View\Components', 'napadon-admin');

        // ลงทะเบียน Livewire
        // Livewire::namespace('napadon-admin', 'Napadon\Admin\App\Livewire');
        // Livewire::component('napadon-admin::anatomy.authentication.basic-authentication.login', \Napadon\Admin\App\Livewire\Anatomy\Authentication\BasicAuthentication\Login::class);
        // Livewire::component('napadon-admin::dashboard.default-widget',      \Napadon\Admin\App\Livewire\Dashboard\DefaultWidget::class);
        // Livewire::component('napadon-admin::modal.user-selector',           \Napadon\Admin\App\Livewire\Modal\UserSelector::class);
        // Livewire::component('napadon-admin::user.navbar-profile',           \Napadon\Admin\App\Livewire\User\NavbarProfile::class);
        // Livewire::component('napadon-admin::user.sidebar-profile',          \Napadon\Admin\App\Livewire\User\SidebarProfile::class);
        // Livewire::component('napadon-admin::organization',                  \Napadon\Admin\App\Livewire\Organization::class);
        // Livewire::component('napadon-admin::organization.tree',             \Napadon\Admin\App\Livewire\Organization\Tree::class);
        // Livewire::component('napadon-admin::organization.default-panel',    \Napadon\Admin\App\Livewire\Organization\DefaultPanel::class);
        // Livewire::component('napadon-admin::organization.user-panel',       \Napadon\Admin\App\Livewire\Organization\UserPanel::class);
        // Livewire::component('napadon-admin::about',                         \Napadon\Admin\App\Livewire\About::class);

        // Livewire::component('napadon-admin::permission-browse',             \Napadon\Admin\App\Livewire\PermissionBrowse::class);

        // Livewire::component('napadon-admin::role-browse',                   \Napadon\Admin\App\Livewire\RoleBrowse::class);
        // Livewire::component('napadon-admin::role-permission-browse',        \Napadon\Admin\App\Livewire\RolePermissionBrowse::class);

        // Livewire::component('napadon-admin::user-browse',                   \Napadon\Admin\App\Livewire\UserBrowse::class);
        // Livewire::component('napadon-admin::user-role-browse',              \Napadon\Admin\App\Livewire\UserRoleBrowse::class);
        // Livewire::component('napadon-admin::user-permission-browse',        \Napadon\Admin\App\Livewire\UserPermissionBrowse::class);

        // ลงทะเบียน View
        // $this->loadViewsFrom(__DIR__.'/resources/views', 'napadon-admin');

        // ไฟล์ที่ต้อง Copy ไปยัง Project ที่ใช้งาน แบบครั้งเดียว ไม่เขียนทับ
        // $this->publishes([
        //     __DIR__.'/../publish/config/napadon-admin.php'                          => \config_path('napadon-admin.php'),                       // ไฟล์ตั้งค่า
        //     __DIR__.'/../publish/lang'                                              => \base_path('lang'),                                      // ไฟล์ภาษา
        //     __DIR__.'/../publish/resources/views/errors'                            => \resource_path('views/errors'),
        //     __DIR__.'/../publish/storage/app/public/user-avatar/default-avatar.jpg' => \storage_path('app/public/user-avatar/default-avatar.jpg')
        // ], 'napadon-admin-once');

        // ไฟล์ที่ต้อง Copy ไปยัง Project ที่ใช้งาน
        $this->publishes([
            __DIR__.'/../publish/tests/Feature/Vendor/Nsru/Health'   => \base_path('tests/Feature/Vendor/Nsru/Health')
        ], 'laravel-assets');

        // ลงทะเบียน Console Command
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
            ]);
        }

        // AboutCommand::add('Napadon Admin', fn () => [
            // 'Route name prefix' => config('napadon-admin.route_name_prefix'),
            // 'Theme'             => config('napadon-admin.theme'),
        // ]);

        // AboutCommand::add('PHP Configuration', fn () => [
        //     'Upload Max Filesize'   => \ini_get('upload_max_filesize'),
        //     'Post Max Size'         => \ini_get('post_max_size'),
        //     'Memory Limit'          => \ini_get('memory_limit'),
        // ]);

    }
}
