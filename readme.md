# README

## การติดตั้ง
1. แก้ไขไฟล์ composer.json ของโปรเจค เพิ่มคำสั่งใน post-update-cmd ดังนี้
```json
{
    "scripts": {
        "post-update-cmd": [
            "@php artisan vendor:publish --tag=nsru-health --force"
        ],
    }
}
```
2. สั่ง `composer update` อีกครั้ง
